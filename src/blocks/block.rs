use std::fmt::Debug;

use crate::{byte_vec_to_u128, Transaction};
use chrono::{DateTime, Utc};
use sha3::{Digest, Keccak512};

pub struct Block {
    transactions: Vec<Transaction>,
    time: DateTime<Utc>,
    hash: Vec<u8>,
    prev_hash: Vec<u8>,
    nonce: u64,
    difficulty: u128,
}

impl Block {
    pub fn new(transactions: Vec<Transaction>, prev_hash: Vec<u8>) -> Self {
        let time = Utc::now();

        Self {
            time,
            transactions,
            prev_hash,
            hash: vec![0; 64],
            nonce: 0,
            difficulty: 0x0000ffffffffffffffffffffffffffff,
        }
    }

    pub fn mine(&mut self) {
        for _ in 0..(u64::MAX) {
            self.hash = self.hash();
            if self.check_difficulty() {
                println!("Block mined!");
                return;
            }
            self.nonce += 1;
        }
    }

    pub fn hash(&self) -> Vec<u8> {
        let mut sha = Keccak512::new();

        sha.update(self.time.to_string());
        sha.update(&self.prev_hash);
        sha.update(self.nonce.to_be_bytes());
        sha.update(self.difficulty.to_be_bytes());

        for t in &self.transactions {
            sha.update(t.get_hash());
        }

        sha.finalize().to_vec()
    }

    pub fn is_valid(&self) -> bool {
        self.hash() == self.hash
    }

    pub fn get_hash(&self) -> &Vec<u8> {
        &self.hash
    }

    pub fn transaction_len(&self) -> usize {
        self.transactions.len()
    }

    pub fn calculate_fee_total(&self) -> f32 {
        let mut result: f32 = 0.0;
        for t in &self.transactions {
            result += t.get_fee();
        }

        result
    }

    fn check_difficulty(&self) -> bool {
        byte_vec_to_u128(&self.hash) < self.difficulty
    }
}

impl Debug for Block {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{{ time: \"{}\", prev_hash: \"{}\", hash: \"{}\", nonce: {}, difficulty: {}, transactions: {:?} }}",
            self.time,
            hex::encode(&self.prev_hash),
            hex::encode(&self.hash),
            self.nonce,
            self.difficulty,
            self.transactions
        )
    }
}

use openssl::rsa::Rsa;
use std::{
    fs::File,
    io::{Read, Write},
    path::Path,
};

pub fn keys_exist(public: &str, private: &str) -> bool {
    Path::exists(Path::new(public)) && Path::exists(Path::new(private))
}

pub fn generate_keys(public: &str, private: &str) -> (Vec<u8>, Vec<u8>) {
    let rsa = Rsa::generate(4096).unwrap();
    let pri_bytes = rsa.private_key_to_pem().unwrap();
    let pub_bytes = rsa.public_key_to_pem().unwrap();

    if let Ok(mut file) = File::create(Path::new(public)) {
        file.write_all(&pub_bytes)
            .expect("Failed to write public key to file");
    }

    if let Ok(mut file) = File::create(Path::new(private)) {
        file.write_all(&pri_bytes)
            .expect("Failed to write private key to file");
    }

    (pub_bytes, pri_bytes)
}

pub fn read_keys(public: &str, private: &str) -> (Vec<u8>, Vec<u8>) {
    let mut pub_bytes: Vec<u8> = vec![];
    let mut pri_bytes: Vec<u8> = vec![];

    if let Ok(mut file) = File::open(Path::new(public)) {
        file.read_to_end(&mut pub_bytes)
            .expect("Failed to read public key file");
    }

    if let Ok(mut file) = File::open(Path::new(private)) {
        file.read_to_end(&mut pri_bytes)
            .expect("Failed to read private key file");
    }

    (pub_bytes, pri_bytes)
}

use std::fmt::Debug;

use chrono::{DateTime, Utc};
use sha3::{Digest, Keccak512};

#[derive(Clone)]
pub struct Transaction {
    sender: String,
    receiver: String,
    time: DateTime<Utc>,
    amount: f32,
    fee: f32,
    hash: Vec<u8>,
}

impl Transaction {
    pub fn new(sender: String, receiver: String, amount: f32, fee: f32) -> Self {
        let time = Utc::now();
        let mut sha = Keccak512::new();

        sha.update(&sender);
        sha.update(&receiver);
        sha.update(amount.to_be_bytes());
        sha.update(time.to_string());
        sha.update(fee.to_be_bytes());

        let hash = sha.finalize();

        Self {
            sender,
            receiver,
            time,
            amount,
            fee,
            hash: hash.to_vec(),
        }
    }

    pub fn get_hash(&self) -> &Vec<u8> {
        &self.hash
    }

    pub fn get_fee(&self) -> f32 {
        self.fee
    }
}

impl Debug for Transaction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{ sender: \"{}\", receiver: \"{}\", time: \"{}\", amount: {}, fee: {}, hash: \"{}\" }}",
            self.sender,
            self.receiver,
            self.time,
            self.amount,
            self.fee,
            hex::encode(&self.hash)
        )
    }
}

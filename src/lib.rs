pub mod blockchain;
pub mod blocks;
pub mod key_generator;
pub mod threadpool;
pub mod transactions;

pub use blockchain::Blockchain;
pub use blocks::Block;
pub use transactions::Transaction;

use std::u128;

pub fn byte_vec_to_u128(bytes: &Vec<u8>) -> u128 {
    (bytes[0] as u128) << 0x0 * 8
        | (bytes[1] as u128) << 0x1 * 8
        | (bytes[2] as u128) << 0x2 * 8
        | (bytes[3] as u128) << 0x3 * 8
        | (bytes[4] as u128) << 0x4 * 8
        | (bytes[5] as u128) << 0x5 * 8
        | (bytes[6] as u128) << 0x6 * 8
        | (bytes[7] as u128) << 0x7 * 8
        | (bytes[8] as u128) << 0x8 * 8
        | (bytes[9] as u128) << 0x9 * 8
        | (bytes[10] as u128) << 0xa * 8
        | (bytes[11] as u128) << 0xb * 8
        | (bytes[12] as u128) << 0xc * 8
        | (bytes[13] as u128) << 0xd * 8
        | (bytes[14] as u128) << 0xe * 8
        | (bytes[15] as u128) << 0xf * 8
}

use sand_dollar::threadpool::ThreadPool;
use sand_dollar::Block;
use std::env;
use std::io::Write;
use std::net::TcpListener;
use std::net::TcpStream;
use std::sync::{Arc, Mutex};

fn main() {
    let address = Box::new("My Address".to_string());

    let handle = std::thread::spawn(move || {
        for _ in 0..10 {
            println!("My Address is: {}", address);
        }
    });

    // println!("{}", address);

    handle.join().unwrap();
}

// fn handle_connection() {
//     println!("Hello!");
// }

/*
 let args: Vec<String> = env::args().collect();

    if args.len() == 1 {
        println!("Missing arguments");
        return;
    }

    // let listener = TcpListener::bind(&args[1]).unwrap();
    let pool = ThreadPool::new(4);

    let hashes = Arc::new(Mutex::new(vec![]));

    for _ in 0..4 {
        let hashes = Arc::clone(&hashes);
        pool.execute(|| {
            handle_connection(hashes);
        })
    }

    //std::thread::sleep(std::time::Duration::new(30, 0));

    let hs = hashes.lock().unwrap().to_vec();
    for h in hs {
        println!("HashCode: {}", &h);
    }
    // for stream in listener.incoming() {
    //     let mut stream = stream.unwrap();

    //     pool.execute(|| {
    //         handle_connection(stream);
    //     });

    //     println!("Connection established!");
    // }
}

fn handle_connection(hashes: Arc<Mutex<Vec<String>>>) {
    let mut blocks = vec![
        Block::new(vec![], vec![0; 64]),
        Block::new(vec![], vec![0; 64]),
        Block::new(vec![], vec![0; 64]),
        Block::new(vec![], vec![0; 64]),
    ];

    for b in &mut blocks {
        b.mine();
        hashes.lock().unwrap().push(hex::encode(b.get_hash()))
    }
*/

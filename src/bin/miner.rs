use sand_dollar::key_generator::{generate_keys, keys_exist, read_keys};
use sand_dollar::{Blockchain, Transaction};
use sha3::{Digest, Keccak512};

fn main() {
    let pub_path: &str = "public.key";
    let pri_path: &str = "private.key";

    let mut keys: (Vec<u8>, Vec<u8>) = (vec![], vec![]);
    if !keys_exist(pub_path, pri_path) {
        keys = generate_keys(pub_path, pri_path);
    } else {
        keys = read_keys(pub_path, pri_path);
    }

    let address = create_address(keys.0);

    println!("{}", &address);

    let mut blockchain = Blockchain::new();
    for i in 0..10 {
        blockchain.add_transaction(Transaction::new(
            "Sender".to_string(),
            "Receiver".to_string(),
            i as f32,
            0.001,
        ));
    }

    for _ in 0..10 {
        let mut block = blockchain.generate_block();
        block.mine();

        blockchain.add_block(block, &address);
    }

    println!("{:?}", &blockchain);
}

fn create_address(pub_key: Vec<u8>) -> String {
    let mut sha = Keccak512::new();

    sha.update(pub_key);

    format!("{}{}", "Sand", hex::encode(sha.finalize()))
}

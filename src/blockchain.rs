use crate::{Block, Transaction};
use std::fmt::Debug;

const MAX_TX_PER_BLOCK: usize = 10;
const COIN_BASE: usize = 10;

pub struct Blockchain {
    pending_transactions: Vec<Transaction>,
    chain: Vec<Block>,
}

impl Blockchain {
    pub fn new() -> Self {
        Self {
            pending_transactions: vec![],
            chain: vec![],
        }
    }

    pub fn add_transaction(&mut self, t: Transaction) {
        // Validate transaction first.
        self.pending_transactions.push(t);
    }

    pub fn generate_block(&self) -> Block {
        let mut transactions = vec![];
        if self.pending_transactions.len() > MAX_TX_PER_BLOCK {
            for i in 0..MAX_TX_PER_BLOCK {
                transactions.push(self.pending_transactions[i].clone());
            }
        } else {
            for i in 0..(self.pending_transactions.len()) {
                transactions.push(self.pending_transactions[i].clone());
            }
        }

        if let Some(last_block) = self.chain.last() {
            Block::new(transactions, last_block.get_hash().to_vec())
        } else {
            Block::new(transactions, vec![0; 64])
        }
    }

    pub fn add_block(&mut self, b: Block, address: &String) {
        if b.is_valid() {
            for _ in 0..b.transaction_len() {
                self.pending_transactions.remove(0);
            }

            let reward = COIN_BASE as f32 + b.calculate_fee_total();
            self.chain.push(b);

            self.pending_transactions.push(Transaction::new(
                "Network".to_string(),
                address.to_owned(),
                reward,
                0.0,
            ));
        }

        println!("Successfully mined a block!");

        // Either the block was pushed to the network or not.
        // In either scenario after attempting to upload a block
        // we should query the network again for updates on the chain
        // as a new block could have been added and pending transactions
        // removed as a result.
    }
}

impl Debug for Blockchain {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{ pending_transactions: {:?}, chain: {:?} }}",
            self.pending_transactions, self.chain
        )
    }
}
